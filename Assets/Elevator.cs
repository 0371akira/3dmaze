﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
	public int floor = 2;
	float time = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    	time += 0.01f;
        this.gameObject.transform.Translate(0, (float)Math.Sin(time) * floor / 50, 0);
    }
}
