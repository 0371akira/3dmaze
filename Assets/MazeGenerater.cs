﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerater : MonoBehaviour
{
	public GameObject polePrefab;

    // Start is called before the first frame update
    void Start()
    {
    	for (int f = 1; f <= 13; f++) {
	    	using (StreamReader reader = new StreamReader("Assets/Maze/floor" + f + ".txt")) {
				for (int i = 0; i < 25; i++) {
					string line = reader.ReadLine();
		    	    for (int j = 0; j < 25; j++) {
		    	        if(line[j] == '1') {
		    	        	Instantiate (polePrefab,
		    	        		new Vector3 (i * 4 - 48, f * 4 - 2, j * 4 - 48),
		    	        		Quaternion.identity
		    	        	);
		    	        }
		    	    }
				}
			}
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
