﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public Text timer;
	public Text message;
	static public bool clearFlg = false;
	
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    	if(clearFlg) {
    		return;
    	}
        timer.text = "経過時間：" + (int)Time.time + " 秒";
        // 5秒経過していたら、messageのtextを空文字""にする
				if(Time.time >= 5){
						message.text = "";
				}
    }
}
