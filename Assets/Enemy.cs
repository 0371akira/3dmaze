﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    NavMeshAgent enemy;
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        enemy = gameObject.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
			//	if(enemy.pathStatus != NavMeshPathStatus.PathInvalid) {
        			if (player != null) {
        			    enemy.destination = player.transform.position;
        			}
			//	}
    }
}
