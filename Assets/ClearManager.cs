﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearManager : MonoBehaviour
{
	public Text message;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void OnTriggerEnter(Collider collision)
    {
		if(collision.gameObject.tag == "Goal") {
			// GameManagerのclearFlgをtrueにする
			GetComponent<GameManager>().enabled = true;
			message.text = "迷宮から脱出しました　おめでとう！！";
			GetComponent<Animator>().enabled = false;
			
        }
    }
}
